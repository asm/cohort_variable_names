
# Variable naming scheme

This document provides an overview of the variable naming scheme. 


## Generic naming rules

Variable names should be of the following generic format:

    'subject'_'majorvariable'.'source'_’minorvariable’_'unit'_'timepoint' 

They may, however, use a contracted version of the generic format. For
instance, a majority of variables will not require both a `major` and
`minor` component, and the subject (or other) components may be
implicit. Indeed, the minimum requirement for a variable name is the
component described (above) as the `majorvariable` component,
even the `.source` is optional.

Further detail is provided below and in the linked documents. 

## Variable name length and conventions

Ideally, the variable name should be as short as possible, while still
complying with the [conventions](/background.md#Conventions). It is
recommended that the variable name should be fewer than 30 characters,
including the inter-connecting underscore characters (`_`), although
there will be occasions when it is necessary to have a longer name.

Furthermore, the names should aim to be pronouncable and conform if
possible to well-accepted definitions and names. 

Additional information about follow-up variable names is documented on
the [questionnaires](/questionnaires.md) page.


## Naming rule components

### [`subject`](/subject.md)

This should be specified unless the variable relates to the index
subject (i.e. fetus/baby/child). 

For example `mat_` (for maternal/mother variables), `pat_` (for
paternal/father variables); further options: `matgm_` , `matgf_` ,
`patgm_` , `patgf_` , `sibX_` (should all be self-explanatory!).

Subject: Use child1_, child2_, etc.  for participant’s offspring variables.

Use `offspr` (for offspring) for variables that characterise the
participant on having biological children.

For example, `offspr_bin_18y` (does the participant have any biological
child, as surveyed at the 18 year follow-up).

### [`variable`](/variable.md)

Main stem, representing the variable being described. 

For example, `wgt` , `hgt` , `educ` (all should be self-explanatory). It
could be that there are sub-categories with a variable, which would then
require using a 'major' branch and 'minor' stem. Further, if the
variable relates to a follow-up questionnaire, it may also be necessary
to indicate who the respondent is.

#### [(`majorvariable`)](/variable.md#major-variable-component)

Main root of the variable.

For example, `vent` for variables relating to neonatal ventilation or
respiratory support.

#### [(`source`)](/variable.md#source)

If the variable refers to a follow-up questionnaire, then the
`majorvariable` field refers to the test used and the information
`source` (i.e. whoever the respondent to the questionnaire is, rather
than the subject of the questionnaire) is added after a dot (`.`). This
should be:

 * s = self report (this is only required if the questionnaire could
   also be responded to by somebody else; if it is otherwise implicit,
   it should _not_ be included)
 * p = parent report
 * t = teacher report
 * e = examiner report
 * m = medical report

An example for a follow-up questionnaire might be `hui.p_item1_14y`,
referring to parental report of item 1 of the Health Utilities Index
measured when the subject is seen for the 14 year follow-up.


#### [(`minorvariable`)](/variable.md#minor-variable-component)

Minor branch of variable

For example, `vent_mech` , `vent_ninv`, `vent_cpap`, `vent_oxy` – for,
respectively, mechanical ventilation, non-invasive ventilation,
ventilatory support with CPAP, ventilatory support including oxygen (NB:
these categories are not necessarily exclusive – the subject could be on
mechanical ventilation _and_ require oxygen, for example).

In the case of follow-up questionnaires, this becomes the item name.

### [`unit`](/unit.md)

This specifies the unit/class of the 'variable' option. 

For example, maternal education (at time point X) could be defined in
the number of years education or the ISCED level reached, or using some
other categorisation; these would give, respectively, `mat_educ_yrs_X`
or `mat_educ_isced_X` or `mat_educ_cat_X`.

Another example might be the age of death of the child, in hours, days,
or time periods: `death_age_hrs`, `death_age_days`, `death_age_cat`
(e.g. categories being: pre-maternal admission, during labour, in the
delivery room, in the NICU, post-discharge, alive).

### [`timepoint`](/timepoint.md)

This should be the time point at which the variable is measured or, in
the case of follow-up questionnaires, age of administration of the test;
if is there is an age-range; then agreement should be reached on whether
to use the mean age, an age range or a description. This should be made
clear in the variable definition in the dictionary.

For example, `wgt_g_birth`, `wgt_g_discharge`, `wgt_kg_1y`, for (index
subject's) weight measured at birth (in grams), discharge from hospital
(in grams) or 1 year of age (in kilograms).

Another example might be `mat_educ_yrs_birth`, `mat_educ_yrs_1y`,
`mat_educ_yrs_11y`, for years of maternal education at the time of the
child’s birth, when the child is 1 year old, and when the child is 11
years old.

## Examples

Many examples are provided in the  [`examples`](/examples.md) document.

A more complete list of variables used in questionnaires is provided on
the [questionnaires](/questionnaires.md) page.

