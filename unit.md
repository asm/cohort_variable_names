# Units


This specifies the unit/class of the 'variable' option. 

The objective of the 'unit' section of the variable naming scheme should
be to provide an indication to the reader of the variable name (or, the
statistical code) the exact type of unit or type of classification that
is being used.

## Quantitative variables

There are two basic types of quantitative variable:

 * **integer** (also known as _discrete_ or _interval_) variables: these
   are _whole_ numbers and contain discrete counts comprised of whole
   numbers.
 
 * **continuous** (also known as _ratio_) variables: these are variables
   that may contain fractions of (or decimal) numbers.
 
There are also certain special case scenarios related to quantitative
variables - notably, variables dealing with _time_, and also what we
call 'compound' variables which is where than more than one variable may
be used to describe a single piece of information.
 


### Time

A special note about variables denoting time frames. These are quantitative variables
 
### Integer variables

By default, integer variables should be denoted as `_int_` within the
naming scheme. However, it is likely that more specific terminology
indicating the actual scale used will be appropriate, and where possible
this should be used in preference to the default name.

For example, variables using an integer time component should be
indicated with a standard notation from the following:

 * `_secs_` : seconds
 * `_mins_` : whole minutes
 * `_hrs_` : complete hours
 * `_days_` : whole days
 * `_wks_` : complete weeks
 * `_yrs_` : whole years

### Continuous

By default, continuous variables should be donated as `_cont_` within
the naming scheme. However, as for integer variables, it will normally
be more appropriate to use more specific terminology indicating the
actual scale. Again, this should be used in preference to the default
name.

Using again the example of time components, the unit name would hear
indicate that the variable is measured on a decimal scale. Names should
therefore have a prefix indicating this:

 * `_dmins_` : decimal minutes
 * `_dhrs_` : decimal hours
 * `_ddays_` : decimal days
 * `_dwks_` : decimal weeks
 * `_dyrs_` : decimal years


### Time variables

As already noted, time maybe represented using integer or continuous
scales. The data may also be split into more than one variable: this is
discussed below under the heading [compound
variables](#compound-variables).


However, time is a complicated concept, as [discussed
elsewhere](https://r4ds.had.co.nz/dates-and-times.html).

An important further consideration for time variables is the 'origin',
as discussed in the [`timepoint`](/timepoint.md) section of the naming
scheme; consideration should be given to whether this should also be
indicated for the unit.



### Compound variables

"Compound" variables are continuous data that are split into multiple
variables.  For example, gestational age measured in weeks and days may
be split into one variable containing the number of weeks and a second
variable containing the number of days (0 to 6). Another example could
be the date, with separate variables for year, month (0 to 12) and day
of the month (0 to 31)

In this situation, the second (or higher) variable should contain the
prefix `r` to indicate that it provides a remainder. For example, for
gestational age this would provide:

 * `_gawks_` : number of weeks since mother's last menstrual period
 * `_rdays_`: remaining number of days. Note: this does _not_ include
   '`ga`' as it is simply the remainder of the week, and therefore
   unrelated to a specific time origin.

If combined, these would then logically create `_dgawks_` (for decimal
gestational age, in weeks); however, as it is unlikely that 'decimal
gestational age' would be measured in days, this could be shortened to
`_dga_`.


An alternate example, using time values, would be:

 * `_hrs_` : hour of the day (0 to 23)
 * `_rmins_` : minute of the hour (0 to 59)

These could then combined to create another variable, `_dhrs_` (decimal
hours).


## Categorical variables

Categorical variables may be divided into three types of variable:

 * Binary (or _dichotomous_) variables.
 
 * Nominal variables: these are variables with discrete categories that
   can all be considered equivalent in rank - that is, there is no
   order. Examples include months of the year, or different colours.
 
 * Ordinal (also known as _ordered categorical_) variables: these are
   categorical variables in which there is a hierarchy - for example,
   level of education, or categories used in a Likert classification.

### Binary variables

Binary variables should normally be denoted `_bin_` within the naming
scheme, unless there is a more appropriate notation for that particular
variable.

### Nominal variables

The default for nominal variables is that they should be denoted
`_`_`X`_`nom_` within the naming scheme, where the prefix _`X`_
represents the number of categories. However, there may be a more
appropriate name; if so, this should preferentially be used.


### Ordinal variables

The default for ordinal (ordered categorical) variables is that they
should be denoted `_`_`X`_`ord_` within the naming scheme, where the
prefix _`X`_ represents the number of categories. Again, there may well
be a more appropriate name that should be used in preference.

For example, the International Standard Classification of Education
(ISCED), which was developed by the [UNESCO Institute of
Statistics](http://uis.unesco.org/en/topic/international-standard-classification-education-isced)
and is also recommended by the [European
Union](https://ec.europa.eu/eurostat/statistics-explained/index.php/International_Standard_Classification_of_Education_%28ISCED%29),
is a standardised, ordered classification of education which contains
nine levels of education (from ISCED 0, representing a maximum achieved
level of education below primary education, to ISCED 8, which represents
an education achievement of doctoral level of above), and should be
designated as `_isced_`.

In cases where such an ordered variable is collapsed into a smaller
number of categories, this should be designated using a prefix number
representing the number of categories. For example, the ISCED
categorisation might be collapsed into three categories as follows:

 * level 1: ISCED 0 to ISCED 2
 * level 2: ISCED 3 to ISCED 5
 * level 3: ISCED 6 to ISCED 8
 
The notation for such a variable would be `_3isced_` and the
categorisation would be detailed in the definition of the harmonised
variable.


## Examples

Details of `unit` sections of the variable naming scheme are provided
below. For examples of how they are used within the overall naming
scheme (i.e. in conjunction with the other sections), the reader is
referred to the '[examples](examples.md)' page.

[to be completed]

## Summary

The following default options are available within the 'unit' section of
the variable naming scheme:

 * `_int_`
 * `_cont_`
 * `_bin_`
 * `_nom_`
 * `_ord_`

However, where available, a more appropriate descriptive term should be
used in preference to the default name, as this will aid comprehension
and readability.


---- 

Return to [`naming_scheme`](/naming_scheme.md) main page.
