# Authors and contributors

This documentation was originally authored by:
 
 * [Stef van Buuren](https://stefvanbuuren.name/): <stef.vanbuuren@tno.nl>
 * [Andrei S. Morgan](https://www.andreimorgan.net): <andrei.morgan@inserm.fr>

Additional contributions have been made by:

 * Nicole Baumann <N.Baumann.1@warwick.ac.uk>
 * Helen Collins <hec14@leicester.ac.uk>
 * Robert Eves <R.Eves@warwick.ac.uk>
 * Gonçalo Gonçalves <goncalo.c.goncalves@inesctec.pt>
 * Marina Goulart de Mendonça <M.Goulart-de-Mendonca@warwick.ac.uk>
 * Katriina Heikkilä <katriina.heikkila@thl.fi>
 * Eero Kajantie <eero.kajantie@thl.fi>
 * Eva Liu <Eva.Liu@warwick.ac.uk>
 * Elsa Lorthe <elsa.lorthe@gmail.com>
 * Yanyan Ni <Yanyan.Ni@warwick.ac.uk>
 * Charlotte Powell <charlotte.powell@leicester.ac.uk>
 * Mariane Sentenac <mariane.sentenac@inserm.fr>
 * Sylvia van der Pal <sylvia.vanderpal@tno.nl> 
 
 
