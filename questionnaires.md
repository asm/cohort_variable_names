# Questionnaires

Questionnaires are frequently used in cohort follow-up sweeps and
can present something of a peculiarity.

During childhood for example, questionnaires may be about the index
subject (the child) but could be filled in or completed by the parents,
teachers, examiner - or even the children themselves.

Furthermore, the same questionnaire may have different versions that are
used in different epochs for the same purpose, and it can be important
to differentiate this.

For these reasons, it can be important to include both a version number
and a respondent identifier in the major part of the 'variable'
component of the name - although of course it may also be possible to
omit either one or the other, or both, of these. As questionnaire items
or scores are often standardised, the minor part of the variable should
be used to indicate this. 

