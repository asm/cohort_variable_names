
# Questionnaire codes

The following table shows codes for follow-up questionnaires, to be used
in the [`majorvariable`](/variable.md#major-variable-component)
component of the [variable naming scheme](/naming_scheme.md).



  |  Instrument  |  Acronym  |  suggested variable name acronym  |  length  |  alternative  |  
  | :----------  | :-------  | :-----------------------          | :------: | :------------ |
  |  15-D  |  15-D  |  15d  |  3  |
  |  Five to Fifteen Revised Questionnaire  |  5-15R  |  515r  |  4  |
  |  Adaptive Behaviour Assessment System  |  ABAS  |  abas  |  4  |
  |  DuPaul ADHD Rating Scale-IV  |  ADHD RS-IV  |  adhdrs4  |  7  |  |  dupa4  |  
  |  DuPaul ADHD Rating Scale 5  |  ADHD RS-5  |  adhdrs5  |  7  |  |  dupa5  |  
  |  ADHD self report (Kooij et al) |  |  adhdsr  |  6  |  |  kooij  |  
  |  Autism Diagnostic Interview-Revised  |  ADI-R  |  adir  |  4  |
  |  Autism Diagnostic Observation Schedule  |  ADOS  |  ados  |  4  |
  |  Arnett Inventory of Sensation Seeking  |  AISS  |  aiss  |  4  |
  |  Attentional Network Task  |  ANT  |  ant  |  3  |
  |  Attentional Network Task-II  |  ANT-II  |  antii  |  5  |
  |  Adult Problems Questionnaire  |  APQ  |  apq  |  3  |
  |  Autism Spectrum Quotient   |  AQ  |  aq  |  2  |
  |  Ages and stages Questionnaire, 3rd edition  |  ASQ  |  asq3  |  4  |
  |  Adult Self Report  |  ASR  |  asr  |  3  |
  |  The Adult ADHD Self-Report Scale  |  ASRS  |  asrs  |  4  |
  |  Autism Spectrum Screening Questionnaire  |  ASSQ  |  assq  |  4  |
  |  Alcohol Use Disorders Identification Test  |  AUDIT  |  audit  |  5  |
  |  Automated Working Memory Assessment  |  AWMA  |  awma  |  4  |
  |  Aktiver Wortschatztest  |  AWST  |  awst  |  4  |
  |  Beck Anxiety Inventory  |  BAI  |  bai  |  3  |
  |  Broad Autism Phenotype Questionnaire  |  BAPQ  |  bapq  |  4  |
  |  Beck Depression Inventory  |  BDI  |  bdi  |  3  |
  |  Big Five Inventory-10  |  BFI-10  |  bfi  |  3  |
  |  Behavioural Inhibition System and Behavioural Activation System Scales  |  BIS/BAS scales  |  bisbas  |  6  |  |  bisba  |  
  |  Brief Infant Toddler Behaviour Social Emotional Assessment  |  BITSEA  |  bitsea  |  6  |  |  bitse or bit  |  
  |  Basic Nordic Sleep Questionnaire  |  BNSQ  |  bnsq  |  4  |
  |  Boehm test of basic concepts  |  Boehm-3  |  boehm3  |  6  |  |  boeh3  |  
  |  Behavior Rating Inventory of Executive Function  |  BRIEF  |  brief  |  5  |  |  brie  |  
  |  Behavior Rating Inventory of Executive Functioning -  Adult Version  |  BRIEF-A  |  briefa  |  6  |  |  briea  |  
  |  Brown Attention-Deficit Disorder Scales  |  Brown ADD Scales  |  brown  |  5  |
  |  Brunet-Lézine Scale  |  BL Scale  |  brun  |  4  |
  |  Bayley Scales of Infant Development  |  BSID  |  bsid  |  4  |
  |  Bayley Scales of Infant Development, Second Edition  |  BSID-II  |  bsid2  |  5  |
  |  Beery-Buktenica Developmental Test of Visual-Motor Integration  |  Beery VMI  |  bvmi  |  4  |
  |  Bayley Scales of Infant and Toddler Development, Third Edition  |  Bayley-III  |  bsid3  |  3  |
  |  Cambridge Neuropsychological Test Automated Battery  |  CANTAB  |  cantab  |  6  |  |  canta  |  
  |  Conners Abbreviated Symptom Questionnaire for Parents |  |  casq  |  4  |
  |  Child Behaviour Checklist  |  CBCL  |  cbcl  |  4  |
  |  Child Eating Behaviour Questionnaire  |  CEBQ  |  cebq  |  4  |
  |  Clinical Evaluation of Language Fundamentals  |  CELF-IV  |  celf4  |  5  |
  |  Cognitive Emotional Regulation Questionnaire  |  CERQ  |  cerq  |  4  |
  |  Center for Epidemiologic Studies Depression Scale  |  CES-D  |  cesd  |  4  |
  |  Children’s Global Assessment Scale  |  CGAS  |  cgas  |  4  |
  |  Child Health Questionnaire  |  CHQ  |  chq  |  3  |
  |  Clinical Interview Schedule - Revised  |  CIS-R  |  cisr  |  4  |
  |  Columbia Mental Maturity Scales  |  CMMS  |  cmms  |  4  |
  |  CogHealth 3.0.5 computerised assessment, [Cogstate Ltd, Melbourne Australia](https://www.cogstate.com)  |  |  coghe  |  5  |
  |  Course of Life questionnaire  |  CoLQ  |  colq  |  4  |
  |  COPE inventory (dispositional version)  |  COPE-D  |  coped  |  5  |
  |  Raven's Coloured Progressive Matrices  |  CPM  |  cpm  |  3  |
  |  Conners Continuous Performance Test  |  CPT  |  cpt  |  3  |
  |  Conners Continuous Performance Test-II  |  CPT-II  |  cpt2  |  4  |
  |  Cyberball  |     |  cyber  |  5  |
  |  Diagnostic Analysis of Non Verbal Accuracy  |  DANVA  |  danva  |  5  |
  |  Dyadic Adjustment Scale  |     |  das  |  3  |
  |  Development and Well Being Assessment   |  DAWBA  |  dawba  |  5  |
  |  Disruptive Behaviour Disorders Rating Scales  |  DBDRS  |  dbdrs  |  5  |
  |  Developmental Coordination Disorder Questionnaire  |  DCDQ  |  dcdq  |  4  |
  |  Denver Developmental Screening Test  |  DDST  |  ddst  |  4  |
  |  Digit Span Task |  |  digit  |  5  |
  |  Delis-Kaplan Executive Function System  |  D-KEFS  |  dkefs  |  5  |
  |  Dyslexia Screening Test  |  DST  |  dst  |  3  |
  |  Buss and Plomin Emotionality Activity Sociability Temperament Survey  |  EAS Temperament Survey  |  easts  |  5  |
  |  Experiences in Close Relationships-Revised  |  ECR-R  |  ecrr  |  4  |
  |  Eating Disorder Inventory-2  |  EDI2  |  edi2  |  4  |
  |  Edinburgh Handedness Inventory  |  EHI  |  ehi  |  3  |
  |  Edinburgh Postnatal Depression Scale |  |  epds  |  4  |
  |  Empathy Quotient  |  EQ  |  eq  |  2  |
  |  Expressive Vocabulary Test, Second Edition  |  EVT-2  |  evt2  |  4  |
  |  Family Adversity Index  |  FAI  |  fai  |  3  |
  |  Frankfurt Test and Training of Facial Affect Recognition  |  FEFA  |  fefa  |  4  |
  |  Food frequency questionnaire (Based on the Finnish Nutrition Database)  |     |  ffq  |  3  |
  |  FINRISK 9-item Physical Activity Questionnaire  |     |  finri  |  5  |
  |  Social Communication Questionnaire/Fragebogen zur Sozialen Kommunikation - Autismus-Screening  |  FSK  |  fsk  |  3  |
  |  Fatigue Severity Scale  |  FSS  |  fss  |  3  |
  |  General Health Questionnaire 12 Item version  |  GHQ-12  |  ghq  |  3  |
  |  Gross Motor Function Classification System  |  GMFCS  |  gmfcs  |  5  |
  |  Grooved Pegboard  |     |  gpeg  |  4  |
  |  Griffiths Scales of Babies’ Abilities  |     |  griff  |  5  |
  |  General Self Efficacy Scale Questionnaire  |     |  gsesq  |  5  |
  |  Harter Self-Perception Profile for Children  |  SPPC  |  sppc  |  4  |
  |  Harter Self-Perception Profile for Adolescents, Revised  |  SPPA-R  |  sppar  |  5  |
  |  Health Utilities Index  |  HUI  |  hui  |  3  |
  |  Health Utilities Index-2  |  HUI2  |  hui2  |  4  |
  |  Health Utilities Index-3  |  HUI3  |  hui3  |  4  |
  |  High-Level Mobility Assessment Tool  |  HiMAT  |  himat  |  5  |
  |  Home Observation Measurement of the Environment  |  HOME  |  home  |  4  |
  |  Hospital Anxiety and Depression Scale  |  Hospital Anxiety and Depression Scale  |  hads  |  4  |
  |  Cook-Medley Hostility Scale  |  Ho Scale  |  hos  |  3  |
  |  Bates Infant Characteristics Questionnaire  |  ICQ  |  icq  |  3  |
  |  Inventaires Français du Développement Communicatif  |  Macarthur-Bates CDI/IFDC  |  ifdc  |  4  |
  |  Impact on Family Questionnaire  |     |  ifq  |  3  |
  |  International Physical Activity Questionnaire  |  IPAQ  |  ipaq  |  4  |
  |  International Study on Asthma and Allergy in Childhood Questionnaire  |  ISAAC  |  isaac  |  5  |
  |  Kaufman Assessment Battery for Children  |  KABC  |  kabc  |  4  |
  |  Kaufman Assessment Battery for Children, Second Edition  |  KABC-II  |  kabc2  |  5  |
  |  KIDSCREEN  |  KIDSCREEN  |  kidsc  |  5  |
  |  Kuopio Ischaemic Heart Disease Study 12 month and 24 h physical activity questionnaires  |     |  kihd  |  4  |
  |  Knox cube  |     |  knox  |  4  |
  |  Kiddie Schedule for Affective Disorders and Schizophrenia for School-Age Children – Present and Lifetime Version  |  K-SADS-PL  |  ksads  |  5  |
  |  Klein Sexual Orientation Grid  |     |  ksog  |  4  |
  |  London Handicap Scale  |  LHS  |  lhs  |  3  |
  |  Life Orientation Test- Revised  |  LOT-R  |  lotr  |  4  |
  |  Logopädisher Sprachverständnis Test  |  LSVTA  |  lsvta  |  5  |
  |  Movement Assessment Battery for Children  |  Movement ABC  |  mabc  |  4  |
  |  Movement Assessment Battery for Children, Second Edition  |  Movement ABC-2  |  mabc2  |  5  |
  |  Manual Ability Classification System  |  MACS  |  macs  |  4  |
  |  Macarthur-Bates Communicative Development Inventory- short form  |  MB-CDI  |  mbcdi  |  5  |
  |  Modified Checklist for Autism in Toddlers  |  mCHAT  |  mchat  |  5  |
  |  Munich Composite International Diagnostic Interview  |  M-CIDI  |  mcidi  |  5  |
  |  Mannheimer Eltern Interview/Mannheimer Parent Interview  |  MEI  |  mei  |  3  |
  |  Morningness-Eveningness Questionnaire  |     |  meq  |  3  |
  |  Five Item Mental Health Inventory  |  MHI-5  |  mhi  |  3  |
  |  Mini-International Neuropsychiatric Interview  |  M.I.N.I. Plus  |  minip  |  5  |
  |  MacArthur Scale of Subjective Social Status  |     |  mssss  |  5  |
  |  N-Back test  |  n-back  |  nback  |  5  |
  |  NEO-Personality Inventory  |  NEO-PI  |  neo  |  3  |
  |  NEPSY  |  NEPSY  |  nepsy  |  5  |  |  neps  |  
  |  NEPSY, Second Edition  |  NEPSY-II  |  nepsy2  |  6  |  |  neps2  |  
  |  Nijmeegse ouderlijke stress index – kort  |  NOSIK  |  nosik  |  5  |
  |  Karasek Job Content Questionnaire/Occupational Stress Questionnaire  |  OSQ  |  osq  |  3  |
  |  Paced Auditory Serial Addition Test  |  PASAT  |  pasat  |  5  |
  |  Pain Coping Questionnaire  |  PCQ  |  pcq  |  3  |
  |  Pain Catastrophising Scale  |  PCS  |  pcs  |  3  |
  |  Parent Behavior Inventory  |  |  pbinv  |  5  |
  |  The Parent Report of Children’s Abilities  |  PARCA  |  parca  |  5  |
  |  The Parent Report of Children’s Abilities, Revised for preterm infants  |  PARCA-R  |  parcar  |  6  |
  |  Parent-Infant Relationship Index  |  PIRI  |  piri  |  4  |
  |  Parental Bonding Instrument  |  PBI  |  pbi  |  3  |
  |  Parenting Stress Index   |  |  parsi  |  5  |
  |  Peabody Developmental Motor Scales  |  PDMS  |  pdms  |  4  |
  |  Peabody Picture Vocabulary Test, Fourth Edition  |  PPVT-IV  |  ppvt4  |  5  |
  |  Pediatric Quality of Life Inventory  |  PedsQL  |  pedsql  |  6  |  |  pedsq  |  
  |  Peters Delusions Inventory  |     |  pdi  |  3  |
  |  Psychosis like Symptoms Interview  |  PLIKSi  |  pliks  |  5  |
  |  Premenstral symptoms (Endicott J, Nee J, Cohen J, Halbreich U)   |  |  premen  |  6  |  |  preme  |  
  |  Parental Sensitivity Assessment Scale  |  PSAS  |  psas  |  4  |
  |  Psychosocial Stress Index  |  PSI  |  psi  |  3  |
  |  Paediatric Stroke Outcome Measure  |  PSOM  |  psom  |  4  |
  |  Quality of Life Scale Burckhardt & Anderson (2003)  |  QOLS  |  qols  |  4  |
  |  Questionnaire for Identifying Children with Chronic Conditions  |  QUICCC  |  quiccc  |  6  |  |  quicc  |  
  |  Rapid Automatized Naming/Rapid Alternating Stimulus Tests  |  RAN/RAS  |  ranras  |  6  |  |  ran or ranra  |  
  |  Rey Complex Figure Test  |  RCFT  |  rcft  |  4  |
  |  The Reynell Developmental Language Scales – Third Edition  |  RDLS-III  |  rdls3  |  5  |
  |  Rosenberg Self-Esteem Scale  |  RSES  |  rses  |  4  |
  |  Relational Support Inventory  |  RSI  |  rsi  |  3  |
  |  Regensburg Word Fluency Test  |  RWT  |  rwt  |  3  |
  |  Sociale Angstschaal voor Kinderen  |  SAS-K  |  sask  |  4  |
  |  Screen for Child Anxiety Related Emotional Disorders  |  SCARED  |  scared  |  6  |  |  scare  |  
  |  Scheffzek scoring system  |  |  schef  |  5  |
  |  Symptom Checklist-90-R  |  SCL-90-R  |  scl90r  |  6  |  |  sclr  |  
  |  Social Communication Questionnaire  |  SCQ  |  scq  |  3  |
  |  Strengths and Difficulties Questionnaire  |  SDQ  |  sdq  |  3  |
  |  Short Form-12  |  SF-12  |  sf12  |  4  |
  |  36-Item Short Form Health Survey  |  SF-36  |  sf36  |  4  |
  |  Skogby Sleep Questionnaire  |     |  skog  |  4  |
  |  Stressful Life Events  (2010)  |  |  sle   |  3  |
  |  Swanson, Noland, and Pelham Questionnaire IV  |  SNAP-IV  |  snap4  |  5  |
  |  Symptoms of obsessive-compulsive disorder (OCD) Thomsen PH. Obsessive-compulsive disorder in children and adolescents. Clinical guidelines. Eur Child Adolesc Psychiatry. 1998;7(1):1–11 |  |  socd  |  4  |
  |  Snijders-Oomen Non-Verbal Intelligence Test- Revised  |  SON-R  |  sonr  |  4  |
  |  Schizotypal Personality Questionnaire   |  SPQ-B  |  spqb  |  4  |
  |  Screen for Posttraumatic Stress Symptoms  |  SPTSS  |  sptss  |  5  |
  |  Social Responsiveness Scale  |  SRS  |  srs  |  3  |
  |  Social Support Questionnaire  |  SSQSR  |  ssqsr  |  5  |
  |  Spielberger State Trait Anxiety Inventory  |  STAI  |  stai  |  4  |
  |  Spielberger State Trait Anger Expression Inventory-II  |  STAXI  |  staxi  |  5  |
  |  Stroop Colour Word Interference Test  |  Stroop  |  stroop  |  6  |  |  stroo  |  
  |  Satisfaction With Life Scale  |  SWLS  |  swls  |  4  |
  |  Teacher Academic Attainment Scale  |  TAAS  |  taas  |  4  |
  |  TNO-AZL Questionnaires for Children's Health-Related Quality of Life  |  TACQOL  |  tacqol  |  6  |  |  tacql  |  
  |  Toronto Alexithymia Scale  |  TAS-20  |  tas  |  3  |
  |  TEAM Rating of Adult Behaviour  |     |  teama  |  5  |
  |  TEAM Rating of Child Behaviour  |     |  teamc  |  5  |
  |  Traumatic Experiences Checklist  |  TEC  |  tec  |  3  |
  |  The Trail Making Test  |  TMT  |  tmt  |  3  |
  |  Test of Motor Impairment  |     |  tomi  |  4  |
  |  Touwen Neurological Examination  |  Touwen  |  touwen  |  6  |  |  touwe  |  
  |  Test of Word Reading Efficiency  |  TOWRE  |  towre  |  5  |
  |  Tester's Rating of Adult Behaviour  |  TRAB  |  trab  |  4  |
  |  Tester's Rating of Child Behaviour  |  TRCB  |  trcb  |  4  |
  |  Teacher Report Form  |  TRF  |  trf  |  3  |
  |  Tempo-Test-Rekenen  |  TTR  |  ttr  |  3  |
  |  Visual Analogue Self-Esteem Scale  |  VASES  |  vases  |  5  |
  |  Voice Related Quality of Life  |  V-RQOL  |  vrqol  |  5  |
  |  Visual Search and Attention Test  |  VSAT  |  vsat  |  4  |
  |  Vragenlijst voor gedragsproblemen bij kinderen 6-16 jaar  |  VvGK  |  vvgk  |  4  |
  |  Van Wiechen Scheme  |     |  vws  |  3  |
  |  Weschler Adult Intelligence Scales  |  WAIS  |  wais  |  4  |
  |  Weschler Adult Intelligence Scales, Third Edition  |  WAIS-III  |  wais3  |  5  |
  |  Weschler Abbreviated Scale of Intelligence   |  WASI  |  wasi  |  4  |
  |  Wechsler Abbreviated Scale of Intelligence - Second Edition  |  WASI-II  |  wasi2  |  5  |
  |  Wisconsin Card Sorting Test  |   WCST  |  wcst  |  4  |
  |  Warwick Edinburgh Mental Wellbeing Scale  |  WEMWBS  |  wemwbs  |  6  |  |  wemwb  |  
  |  World Health Organisation Quality of Life-BREF  |  WHOQOL-BREF  |  whoqol  |  6  |  |  whoql  |  
  |  Weschler Individual Achievement Test, Second Edition  |  WIAT-II  |  wiat2  |  5  |
  |  Wechsler Intelligence Scale for Children, Revised  |  WISC-III  |  wisc3  |  5  |
  |  Wechsler Intelligence Scale for Children, Fourth Edition  |  WISC-IV  |  wisc4  |  5  |
  |  Wechsler Intelligence Scale for Children, Fifth Edition  |  WISC-V  |  wisc5  |  5  |
  |  Wechsler Intelligence Scale for Children, Third Edition  |  WISC-R  |  wiscr  |  5  |
  |  Woodcock-Johnson Tests of Achievement  |  WJ ACH  |  wjach  |  5  |
  |  Woodcock-Johnson Tests of Cognitive Abilities  |  WJ COG  |  wjcog  |  5  |
  |  Woodcock-Johnson Tests of Oral Language  |  WJ OL  |  wjol  |  4  |
  |  Weschler Memory Scale, Third Edition  |  WMS-III  |  wms3  |  4  |
  |  Wechsler Preschool and Primary Scale of Intelligence  |  WPPSI  |  wppsi  |  5  |  |  wpps  |  
  |  Wechsler Preschool and Primary Scale of Intelligence-Third Edition  |  WPPSI-III  |  wppsi3  |  6  |  |  wpps3  |  
  |  Wechsler Preschool and Primary Scale of Intelligence-Fourth Edition  |  WPPSI-IV  |  wppsi4  |  6  |  |  wpps4  |  
  |  Wechsler Preschool and Primary Scale of Intelligence-Revised  |  WPPSI-R  |  wppsir  |  6  |  |  wppsr  |  
  |  Young Adult Behaviour Checklist  |  YABCL  |  yabcl  |  5  |
  |  Young Adult Self Report  |  YASR  |  yasr  |  4  |
  |  Yale Children's Inventory  |  YCI  |  yci  |  3  |
  |  Youth Self Report  |  YSR  |  yysr  |  3  |
