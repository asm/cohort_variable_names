# Time points

The `timepoint` component needs to be determined in relation to the most
relevant time point for that particular variable. This would normally be
the time point _at which the variable was recorded_.  However, it maybe
that a certain 'sweep' of a cohort study collects data related to
earlier time points. For example, in a birth cohort, the childhood
follow-up at 2 years of age may collect multiple weights that are
recorded in the child's record since initial discharge from hospital
following birth. In this scenario, it would be most appropriate to label
the `timepoint` according to the age at which the weight value is
related, rather than as `_2y` (i.e. which is when the data were
collected).

Furthermore, two types of time points are possible: these could be
'fixed' time points, relating to a fixed point in time (for example,
birth or at 12 years of age), or 'elastic' time points, relating to a
period of time (such as pregnancy, adolescence).

Another consideration for the `timepoint` component of a variable's name
relates to the 'origin'. Normally, the reference point will be birth (of
the index case) and the time point component will refer to chronological
age. However, occasionally it may be necessary to use a different
origin. This is most likely to occur with events during (the mother's)
pregnancy or the neonatal period, or when using corrected (rather than
chronological) age during early childhood. In these situations, one of
the following prefixes should be appended to the `timepoint` component:

 * `_ga`_`X`_ : relating to post-menstrual age
 * `_c`_`X`_ : indicating _corrected_ age


Examples of such variables include `multiple_ord_ga20w` for number of
fetuses during the pregnancy as measured at 20 weeks of gestational age,
or `blind_bin_c2y` for blindness at 2 years corrected age.

Alternatively, a fixed time point may be required in reference to
calendar date (or even time); this should be indicated with a prefix
taken from `y` (for years), `m` (for months), `d` (for days), or `h`
(for hours), `m` (for minutes), `s` (for seconds) - or a combination of
those. For example, volume of births in a hospital in the year 2010
could be indicated as `hosp_births_n_y2010`

Finally, it should also be noted that the `timepoint` component may not be
required for _invariant_ variables. Examples of these include 'subject
identification' or 'sex' (representing the biological variable, as
opposed to 'gender') which do not change over time.

## Fixed

The following are considered 'fixed' time points as they occur at
discrete points in time:

 * maternal admission to hospital (`matadmit`)
 * onset of labour (`onslab` - note: this may precede maternal admission
   to hospital!)
 * birth (`birth`)
 * admission to neonatal unit (`nicadmit`)
 * discharge (`discharge`)
 * death (`death`)
 
Other fixed time points are more obvious, and related to days, months or
years. These would be indicated using the following nomenclature (where
_X_ represents the numeric part of the time point):
 
 * _`_X`_`d`
 * _`_X`_`m`
 * _`_X`_`y`


## Elastic

'Elastic' time points relate to _durations_ of time, which may be
defined flexibly, according to the study requirements:


 * `prepreg`
   : occurring before pregnancy

 * `antenat`
   : occurring during pregnancy. before birth

 * `delivery`
   : occurring around the time of delivery (including events during any resuscitation occurring in the delivery room or other place of birth)

 * `neonat`
   : occurring during the neonatal period (defined as up to 44 weeks of post-menstrual age)

 * `infant`
   : occurring during very early childhood, also known as infancy.

 * `child`
   : occurring during childhood; this would normally be after very early childhood and before adolescence, but may be a wider ranging period

 * `adol`
   : occurring during adolescence

 * `adult`
   : occurring during adulthood; this would normally be defined as being after 18 years of age, but may included younger ages depending upon the study.

 * `ever`
   : an all encompassing time period, including an event occurring at any age during a subjects life.


Each of the time points after the neonatal period (i.e. from `infant`
onwards) should be numbered as there may be more than one definition
that could be used. For example:

 * `adol1` : might be defined as "adolescence, from 11 to 18 years of age, inclusive" for one study
 
 * `adol2` : might be defined as "adolescence, from 13 to 16 years of age, inclusive" for another study.
 
**Note:** these are only examples! As such, they may not correspond to
actual definitions.


---- 

Return to [`naming_scheme`](/naming_scheme.md) main page.
