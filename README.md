# Cohort variable names

Cohort studies collect data about research participants at multiple
points in time, often amassing a multitude of variables relating to the
original index subject and their relatives, covering a multitude of
domains and different time points, and utilising a range of different
units or categorisations.

This repository contains a living documentation project relating to
suggested variables names for use in epidemiological and statistical
analyses related to such population cohort studies. It stems from work
carried out as part of the [RECAP Preterm](https://recap-preterm.eu)
project, a European Union 'Horizon 2020' funded international
collaborative research program.

In brief, the described naming scheme suggests that variables should
follow the folowing generic format:

    'subject'_'majorvariable'.'source'_’minorvariable’_'unit'_'timepoint' 

They may, however, use a contracted version of the generic format.  Full
information is contained in the [`naming_scheme`](/naming_scheme.md)
document, with further detail and examples of the component parts
provided on other pages. A selection of [examples of complete variable
names](/examples.md) are also provided.

## Version information

**This is currently a preliminary / draft version of the naming scheme.**

The current version of the naming scheme is based on initial drafts
developed by the [authors and contributors](CONTRIBUTORS.md) and can be
roughly considered to be:

 * **Version: 1.1**

The following is a rough guide to versions:

 * Versions 0.x : based on theoretical discussion about best naming
   options
   - Versions up to 0.5 : private versions (within the RECAP Preterm
     project)
   - Version 0.6 : first public version
   
 * Versions 1.x : development versions of the variable naming scheme,
   for use in initial projects.

 * Versions 2.x or higher : stable versions of the variable naming
   scheme, suitable for use in preterm and child birth cohorts.
   
A full [Changelog](CHANGELOG.md) of versions is also provided, along
with a '[todo](TODO.md)' list for consideration in later versions.
   
## Copyright and contributors

This documentation is licensed by the [Authors and
Contributors](/CONTRIBUTORS.md) under the [GNU Free Documentation
License v1.3](/LICENSE.md) as indicated in the [copyright
notice](/COPYRIGHT.md).
