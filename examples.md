
# Examples

This page contains examples of variable names adapted from the EPIPAGE-2
data set using the [cohort variable naming scheme](/naming_scheme.md).


```
## -----------------------------------------------------------------------------
##
## ID VARIABLES
##
## -----------------------------------------------------------------------------

## fetal id
enfants$id <- enfants$id
## enfants$id <- NULL

## maternal id
enfants$mat_id <- enfants$enr_reg_fam
enfants$enr_reg_fam <- NULL

## hospital id
enfants$hosp_id_birth <- enfants$enr_numetabnaiss
enfants$enr_numetabnaiss <- NULL

## region id
enfants$region_id_birth <- enfants$enr_numregnaiss
enfants$enr_numregnaiss <- NULL

## -----------------------------------------------------------------------------
##
## VITAL STATUS / BASELINE INFO / 'CLEANED' VARIABLES
## 
## -----------------------------------------------------------------------------

enfants$wgt_g_birth <- enfants$cor_pds_nce
enfants$cor_pds_nce <- NULL

enfants$sex_3nom <- enfants$cor_sexe_enfant
enfants$cor_sexe_enfant <- NULL

enfants$ga_wks_birth <- enfants$cor_agn
enfants$cor_agn <- NULL

enfants$ga_rdays_birth <- enfants$cor_agn_j
enfants$cor_agn_j <- NULL

enfants$time_hrs_birth <- enfants$cor_heure_naiss
enfants$cor_heure_naiss <- NULL

enfants$time_rmins_birth <- enfants$cor_min_naiss
enfants$cor_min_naiss <- NULL

enfants$age_dhrs_death <- enfants$agedcheure
enfants$agedcheure <- NULL

enfants$age_days_death <- enfants$agedcjour
enfants$agedcjour <- NULL

enfants$age_mins_death <- enfants$agedcminute
enfants$agedcminute <- NULL

## vital status - 5 classes (ordered)
enfants$vstat_5ord <- enfants$cor_statutnais
enfants$cor_statutnais <- NULL

## death between discharge and 2 years
enfants$dth_nicu2y_bin <- enfants$fu2_decesapsortie
enfants$fu2_decesapsortie <- NULL

## death between discharge and 5 years
enfants$dth_nicu5y_bin <- enfants$cor_decesapsortie
enfants$cor_decesapsortie <- NULL



enfants$mat_age_yrs_birth <- enfants$cor_age_mere
enfants$cor_age_mere <- NULL

enfants$fam_ses_6ord_birth <- enfants$cor_cspmenage
enfants$cor_cspmenage <- NULL

enfants$mat_birth_country_5nom <- enfants$cor_pays_naiss
enfants$cor_pays_naiss <- NULL


## -----------------------------------------------------------------------------
##
## MATERNAL VARIABLES
##
## -----------------------------------------------------------------------------

## histological chorioamnionitis
enfants$chorio_histo_bin <- enfants$mere_anapath
enfants$mere_anapath <- NULL

## clinically diagnosed chorioamnionitis
enfants$chorio_clin_bin <- enfants$mere_chorio_clinique
enfants$mere_chorio_clinique <- NULL


enfants$mat_hmrhge_perpart_bin <- enfants$mere_contexte_hemo
enfants$mere_contexte_hemo <- NULL

enfants$abruption_bin <- enfants$mere_contexte_hrp
enfants$mere_contexte_hrp <- NULL

## enfants$ <- enfants$mere_contexte_infect_chorio_4cl
## enfants$mere_contexte_infect_chorio_4cl <- NULL

enfants$rom_prem_bin <- enfants$mere_contexte_rpm
enfants$mere_contexte_rpm <- NULL

## full course of antenatal steroids 
enfants$steroids_full_bin_antenat <- enfants$mere_curecomp
enfants$mere_curecomp <- NULL

enfants$mat_nationality_5nom_birth <- enfants$mere_ma11
enfants$mere_ma11 <- NULL

enfants$mat_insure_4nom_birth <- enfants$mere_ma12
enfants$mere_ma12 <- NULL

enfants$mat_cohabit_bin_birth <- enfants$mere_ma13
enfants$mere_ma13 <- NULL

enfants$mat_work_5nom_birth <- enfants$mere_ma14
enfants$mere_ma14 <- NULL

enfants$mat_hgt_cm <- enfants$mere_mb1
enfants$mere_mb1 <- NULL

enfants$mat_wgt_kg_prepreg <- enfants$mere_mb2
enfants$mere_mb2 <- NULL

enfants$parity_n_birth <- enfants$mere_mb17
enfants$mere_mb17 <- NULL

enfants$mat_bp_bin_prepreg <- enfants$mere_mb5
enfants$mere_mb5 <- NULL

enfants$mat_dm_bin_prepreg <- enfants$mere_mb6
enfants$mere_mb6 <- NULL

enfants$mat_care_reg_bin_antenat <- enfants$mere_mc2
enfants$mere_mc2 <- NULL

enfants$mat_smok_bin_antenat <- enfants$mere_mc3
enfants$mere_mc3 <- NULL

enfants$steroids_any_bin_antenat <- enfants$mere_mi81
enfants$mere_mi81 <- NULL


## In EPIPAGE, maternal admission to hospital is defined as the DELIVERY
## hospital (i.e. after transfer if she was first admitted to a
## different hospital closer to home)
enfants$date_gawks_matadmit <- enfants$mere_mj5a
enfants$mere_mj5a <- NULL

enfants$date_rdays_matadmit <- enfants$mere_mj5b
enfants$mere_mj5b <- NULL

enfants$date_dga_matadmit <- enfants$date_gawks_matadmit + enfants$date_rdays_matadmit/7

## Time before delivery of maternal admission to (delivery) hospital
enfants$time_predel_mins_matadmit <- enfants$mere_mj9a
enfants$mere_mj9a <- NULL


enfants$tfr_bin_antenat <- enfants$mere_mj13
enfants$mere_mj13 <- NULL

enfants$mat_admit_pretfr_hrs_antenat <- enfants$mere_mj18
enfants$mere_mj18 <- NULL

enfants$mat_admit_pretfr_days <- enfants$mere_mj19
enfants$mere_mj19 <- NULL

enfants$rom_date_gawks <- enfants$mere_mj33a
enfants$mere_mj33a <- NULL

enfants$rom_date_rdays <- enfants$mere_mj33b
enfants$mere_mj33b <- NULL

enfants$rom_duratn_mins <- enfants$mere_mj37a
enfants$mere_mj37a <- NULL

enfants$mat_pet_bin <- enfants$mere_mj61
enfants$mere_mj61 <- NULL

enfants$abruption_bin <- enfants$mere_mj73
enfants$mere_mj73 <- NULL

enfants$tocol_any_bin_ever <- enfants$mere_mj112
enfants$mere_mj112 <- NULL

enfants$mat_abx_bin_antenat <- enfants$mere_mj149
enfants$mere_mj149 <- NULL

enfants$mat_mgso4_bin <- enfants$mere_mj183
enfants$mere_mj183 <- NULL

enfants$mat_mgso4_3nom <- enfants$mere_mj184
enfants$mere_mj184 <- NULL


enfants$delplace_home_bin_birth <- enfants$mere_ml1
enfants$mere_ml1 <- NULL

enfants$delplace_transport_bin_birth <- enfants$mere_ml2
enfants$mere_ml2 <- NULL

enfants$lab_onset_3nom <- enfants$mere_ml12
enfants$mere_ml12 <- NULL

enfants$tocol_any_bin_onslab <- enfants$mere_ml20 ## should this be onslab ?
enfants$mere_ml20 <- NULL

enfants$chorio_histo_3nom <- enfants$mere_mn3_resume
enfants$mere_mn3_resume <- NULL

enfants$parity_3nom_birth <- enfants$mere_paritecl
enfants$mere_paritecl <- NULL

enfants$steroids_rescue_bin_antenat <- enfants$mere_rescue
enfants$mere_rescue <- NULL

enfants$steroids_7nom1_antenat <- enfants$mere_statutcan
enfants$mere_statutcan <- NULL


enfants$rxstop_age_days <- enfants$neo_no11b
enfants$neo_no11b <- NULL

enfants$wgt_lt10_bin_birth <- enfants$neo_pn10p_epope
enfants$neo_pn10p_epope <- NULL

enfants$wgt_lt3_bin_birth <- enfants$neo_pn3p_epope
enfants$neo_pn3p_epope <- NULL

enfants$wgt_perc_birth <- enfants$neo_pnperc_epope
enfants$neo_pnperc_epope <- NULL

enfants$wgt_z_birth <- enfants$neo_pnzscore_epope
enfants$neo_pnzscore_epope <- NULL

enfants$sample_ga_weight <- enfants$ponder
enfants$ponder <- NULL

enfants$hosp_births_lt32_n_y2010 <- enfants$emat_a12
enfants$emat_a12 <- NULL




enfants$conganom_4nom <- enfants$enfant_malfo_majeur
enfants$enfant_malfo_majeur <- NULL

enfants$conganom_top_bin <- enfants$enfant_malfofoetale
enfants$enfant_malfofoetale <- NULL

enfants$conganom_lt_bin <- enfants$enfant_malfofoetale2
enfants$enfant_malfofoetale2 <- NULL

enfants$alive_3nom_matadmit <- enfants$enfant_mfiuadmission
enfants$enfant_mfiuadmission <- NULL

enfants$alive_3nom_onslab <- enfants$enfant_mfiutravail
enfants$enfant_mfiutravail <- NULL

enfants$mode_top_bin_delivery <- enfants$enfant_mg12
enfants$enfant_mg12 <- NULL

enfants$mode_3nom_delivery <- enfants$enfant_mm11
enfants$enfant_mm11 <- NULL

enfants$cs_why1_7nom <- enfants$enfant_mm28
enfants$enfant_mm28 <- NULL
enfants$cs_why2_7nom <- enfants$enfant_mm29
enfants$enfant_mm29 <- NULL

enfants$dthage_sb_gawks <- enfants$enfant_mo5a
enfants$enfant_mo5a <- NULL

enfants$dthage_sb_rdays <- enfants$enfant_mo5b
enfants$enfant_mo5b <- NULL

enfants$resus_bin_delivery <- enfants$enfant_mp11
enfants$enfant_mp11 <- NULL

enfants$noresus_reason_3nom <- enfants$enfant_mp12
enfants$enfant_mp12 <- NULL

## enfants$x <- enfants$enfant_mp13
enfants$enfant_mp13 <- NULL

enfants$resus_o2_bin_delivery <- enfants$enfant_mp14
enfants$enfant_mp14 <- NULL

enfants$resus_cpap_bin_delivery <- enfants$enfant_mp15
enfants$enfant_mp15 <- NULL

enfants$resus_ett_bin_delivery <- enfants$enfant_mp16
enfants$enfant_mp16 <- NULL

enfants$resus_cpr_bin_delivery <- enfants$enfant_mp26
enfants$enfant_mp26 <- NULL

enfants$resus_adr_bin_delivery <- enfants$enfant_mp27
enfants$enfant_mp27 <- NULL

enfants$resus_bolus_bin_delivery <- enfants$enfant_mp28
enfants$enfant_mp28 <- NULL

enfants$resus_stop_bin_delivery <- enfants$enfant_mp34
enfants$enfant_mp34 <- NULL



enfants$death_bin_neonat <- enfants$neo_no1
enfants$neo_no1 <- NULL

enfants$death_reason_bin_neonat <- enfants$neo_no2
enfants$neo_no2 <- NULL

enfants$death_bin_delivery <- enfants$enfant_mp38
enfants$enfant_mp38 <- NULL

enfants$apgar5_int <- enfants$enfant_mp9
enfants$enfant_mp9 <- NULL

## enfants$x <- enfants$enr_epipage2
enfants$enr_epipage2 <- NULL

enfants$delplace_nothosp_bin_birth <- enfants$enr_horsmaternite
enfants$enr_horsmaternite <- NULL

enfants$hosp_level_4ord_birth <- enfants$enr_niveau2naiss
enfants$enr_niveau2naiss <- NULL

enfants$consent_3nom_birth <- enfants$enr_participe
enfants$enr_participe <- NULL

enfants$order_int_birth <- enfants$enr_rang
enfants$enr_rang <- NULL

enfants$consent_int_1y <- enfants$enr_suivi1an
enfants$enr_suivi1an <- NULL

## multiple status at start of pregnancy
## enfants$multiple_ord_antenat <- enfants$mere_mc32
## enfants$mere_mc32 <- NULL

## enfants$multiple_ord_ga20w

## multiple status at birth
enfants$multiple_int_birth <- enfants$enr_type_naissance
enfants$enr_type_naissance <- NULL




## 5 years

## relating to whether followed-up or not
enfants$consent_int_5y <- enfants$fu5_asuivre5ans
enfants$fu5_asuivre5ans <- NULL

enfants$contacted_bin_5y <- enfants$fu5_contact
enfants$fu5_contact <- NULL

enfants$responded_bin_5y <- enfants$fu5_repondant
enfants$fu5_repondant <- NULL

enfants$fup_sweep_bin_5y <- enfants$fu5_volet
enfants$fu5_volet <- NULL

## questionnaires available
enfants$medq_bin_5y <- enfants$fu5_qmed
enfants$fu5_qmed <- NULL

enfants$parq_visit_bin_5y <- enfants$fu5_qpar1
enfants$fu5_qpar1 <- NULL

enfants$parq_mail_bin_5y <- enfants$fu5_qpar2
enfants$fu5_qpar2 <- NULL

enfants$psyq_bin_5y <- enfants$fu5_qpsy
enfants$fu5_qpsy <- NULL

## age of exam/questionnaire
enfants$medq_age_mths_5y <- enfants$fu5_v1_premed5b
enfants$fu5_v1_premed5b <- NULL

enfants$parq_age_mths_5y <- enfants$fu5_v1_qpar1b
enfants$fu5_v1_qpar1b <- NULL

enfants$psyq_age_mths_5y <- enfants$fu5_v1_preneuro5
enfants$fu5_v1_preneuro5 <- NULL



## composite variables

enfants$deaf_4ord_5y <- enfants$fu5_defiaudi5ans
enfants$fu5_defiaudi5ans <- NULL

enfants$blind_4ord_5y <- enfants$fu5_defivisu5ans
enfants$fu5_defivisu5ans <- NULL

enfants$school_3nom_5y <- enfants$fu5_ecole
enfants$fu5_ecole <- NULL

enfants$ndis_class_4ord2_5y <- enfants$fu5_overall2
enfants$fu5_overall2 <- NULL

enfants$ndis_class_4ord4_5y <- enfants$fu5_overall4
enfants$fu5_overall4 <- NULL

enfants$cp_bin_5y <- enfants$fu5_pc5ans
enfants$fu5_pc5ans <- NULL

enfants$cp_4ord_5y <- enfants$fu5_pcmarche5
enfants$fu5_pcmarche5 <- NULL


## SDQ
enfants$sdq_int_5y <- enfants$fu5_sdqglobal
enfants$fu5_sdqglobal <- NULL

enfants$sdq_attention_int_5y <- enfants$fu5_hyperact
enfants$fu5_hyperact <- NULL

enfants$sdq_conduct_int_5y <- enfants$fu5_conduite
enfants$fu5_conduite <- NULL

enfants$sdq_emotion_int_5y <- enfants$fu5_emotion
enfants$fu5_emotion <- NULL

enfants$sdq_peer_int_5y <- enfants$fu5_relation
enfants$fu5_relation <- NULL

enfants$sdq_behav_int_5y <- enfants$fu5_social
enfants$fu5_social <- NULL



## 5y medical probs
enfants$wheeze_ever_bin_5y <- enfants$fu5_v1_med3
enfants$fu5_v1_med3 <- NULL

enfants$wheeze_12m_bin_5y <- enfants$fu5_v1_med4
enfants$fu5_v1_med4 <- NULL

enfants$epilepsy_bin_5y <- enfants$fu5_v1_med16
enfants$fu5_v1_med16 <- NULL

enfants$szrnf_12m_nonfeb_bin_5y <- enfants$fu5_v1_med19
enfants$fu5_v1_med19 <- NULL

enfants$mabc_adj_int_5y <- enfants$fu5_v1_med260
enfants$fu5_v1_med260 <- NULL


## WPPSI-IV
enfants$wppsi4_doneprev6m_bin_5y <- enfants$fu5_v1_neu1
enfants$fu5_v1_neu1 <- NULL

enfants$wppsi4_iq_int_5y <- enfants$fu5_v1_neu31
enfants$fu5_v1_neu31 <- NULL

## enfants$wppsi4_est_5ord_5y <- enfants$fu5_v1_neu31clair
enfants$fu5_v1_neu31clair <- NULL


## NEPSY-II
enfants$nepsy2_age_mths_5y <- enfants$wppsi4_age_mths_5y

enfants$nepsy2_doneprev6m_bin_5y <- enfants$fu5_v1_neu34
enfants$fu5_v1_neu34 <- NULL



## Schooling

enfants$school_attends_bin_5y <- enfants$fu5_v1_par4
enfants$fu5_v1_par4 <- NULL

enfants$school_attends_bin_5y <- enfants$fu5_v1_par4b ## this the corrected (thus, correct!) variable
enfants$fu5_v1_par4b <- NULL

## enfants$x <- enfants$fu5_v1_par5
enfants$fu5_v1_par5 <- NULL

enfants$school_class_7nom_5y <- enfants$fu5_v1_par6
enfants$fu5_v1_par6 <- NULL

enfants$school_class_desc_5y <- enfants$fu5_v1_par7
enfants$fu5_v1_par7 <- NULL

enfants$school_allday_3ord_5y <- enfants$fu5_v1_par8
enfants$fu5_v1_par8 <- NULL

## enfants$x <- enfants$fu5_v1_par14
enfants$fu5_v1_par14 <- NULL
## enfants$x <- enfants$fu5_v1_par15
enfants$fu5_v1_par15 <- NULL
## enfants$x <- enfants$fu5_v1_par16
enfants$fu5_v1_par16 <- NULL
## enfants$x <- enfants$fu5_v1_par18
enfants$fu5_v1_par18 <- NULL
## enfants$x <- enfants$fu5_v1_par19
enfants$fu5_v1_par19 <- NULL
## enfants$x <- enfants$fu5_v1_par20
enfants$fu5_v1_par20 <- NULL

enfants$mdph_request_bin_5y <- enfants$fu5_v1_par21
enfants$fu5_v1_par21 <- NULL

enfants$mdph_approved_bin_5y <- enfants$fu5_v1_par22
enfants$fu5_v1_par22 <- NULL

## enfants$x <- enfants$fu5_v1_par23
enfants$fu5_v1_par23 <- NULL
## enfants$x <- enfants$fu5_v1_par24
enfants$fu5_v1_par24 <- NULL

enfants$health_qual_4ord_5y <- enfants$fu5_v1_par25
enfants$fu5_v1_par25 <- NULL

enfants$devel_qual_4ord_5y <- enfants$fu5_v1_par26
enfants$fu5_v1_par26 <- NULL

enfants$worry_wgt_bin_5y <- enfants$fu5_v1_par27
enfants$fu5_v1_par27 <- NULL

enfants$worry_sleepresp_bin_5y <- enfants$fu5_v1_par51
enfants$fu5_v1_par51 <- NULL

## enfants$sdq_behavXX_ord_5y <- enfants$fu5_v1_par83
enfants$fu5_v1_par83 <- NULL

## enfants$sdq_emotionXX_ord_5y <- enfants$fu5_v1_par153
enfants$fu5_v1_par153 <- NULL

## enfants$x <- enfants$fu5_v1_par163
enfants$fu5_v1_par163 <- NULL
## enfants$x <- enfants$fu5_v1_par164
enfants$fu5_v1_par164 <- NULL
## enfants$x <- enfants$fu5_v1_par165
enfants$fu5_v1_par165 <- NULL
## enfants$x <- enfants$fu5_v1_par166
enfants$fu5_v1_par166 <- NULL
## enfants$x <- enfants$fu5_v1_par167
enfants$fu5_v1_par167 <- NULL
## enfants$x <- enfants$fu5_v1_par168
enfants$fu5_v1_par168 <- NULL
## enfants$x <- enfants$fu5_v1_par169
enfants$fu5_v1_par169 <- NULL
## enfants$x <- enfants$fu5_v1_par170
enfants$fu5_v1_par170 <- NULL
## enfants$x <- enfants$fu5_v1_par171
enfants$fu5_v1_par171 <- NULL
## enfants$x <- enfants$fu5_v1_par172
enfants$fu5_v1_par172 <- NULL
## enfants$x <- enfants$fu5_v1_par173
enfants$fu5_v1_par173 <- NULL
## enfants$x <- enfants$fu5_v1_par174
enfants$fu5_v1_par174 <- NULL
## enfants$x <- enfants$fu5_v1_par175
enfants$fu5_v1_par175 <- NULL
## enfants$x <- enfants$fu5_v1_par176
enfants$fu5_v1_par176 <- NULL
## enfants$x <- enfants$fu5_v1_par177
enfants$fu5_v1_par177 <- NULL
## enfants$x <- enfants$fu5_v1_par178
enfants$fu5_v1_par178 <- NULL
## enfants$x <- enfants$fu5_v1_par179
enfants$fu5_v1_par179 <- NULL
## enfants$x <- enfants$fu5_v1_par180
enfants$fu5_v1_par180 <- NULL
## enfants$x <- enfants$fu5_v1_par181
enfants$fu5_v1_par181 <- NULL
## enfants$x <- enfants$fu5_v1_par182
enfants$fu5_v1_par182 <- NULL
## enfants$x <- enfants$fu5_v1_par183
enfants$fu5_v1_par183 <- NULL
## enfants$x <- enfants$fu5_v1_par184
enfants$fu5_v1_par184 <- NULL
## enfants$x <- enfants$fu5_v1_par185
enfants$fu5_v1_par185 <- NULL
## enfants$x <- enfants$fu5_v1_par186
enfants$fu5_v1_par186 <- NULL
## enfants$x <- enfants$fu5_v1_par187
enfants$fu5_v1_par187 <- NULL
## enfants$x <- enfants$fu5_v1_par188
enfants$fu5_v1_par188 <- NULL
## enfants$x <- enfants$fu5_v1_par189
enfants$fu5_v1_par189 <- NULL
## enfants$x <- enfants$fu5_v1_par190
enfants$fu5_v1_par190 <- NULL
## enfants$x <- enfants$fu5_v1_par191
enfants$fu5_v1_par191 <- NULL
## enfants$x <- enfants$fu5_v1_par192
enfants$fu5_v1_par192 <- NULL
## enfants$x <- enfants$fu5_v1_par193
enfants$fu5_v1_par193 <- NULL
## enfants$x <- enfants$fu5_v1_par194
enfants$fu5_v1_par194 <- NULL
## enfants$x <- enfants$fu5_v1_par197
enfants$fu5_v1_par197 <- NULL

## enfants$x <- enfants$fu5_v1_par197_2
enfants$fu5_v1_par197_2 <- NULL
## enfants$x <- enfants$fu5_v1_par197_3
enfants$fu5_v1_par197_3 <- NULL
## enfants$x <- enfants$fu5_v1_par197_4
enfants$fu5_v1_par197_4 <- NULL
## enfants$x <- enfants$fu5_v1_par197_5
enfants$fu5_v1_par197_5 <- NULL
## enfants$x <- enfants$fu5_v1_par197_6
enfants$fu5_v1_par197_6 <- NULL

## enfants$x <- enfants$fu5_v1_par197_11
enfants$fu5_v1_par197_11 <- NULL
## enfants$x <- enfants$fu5_v1_par197_12
enfants$fu5_v1_par197_12 <- NULL
## enfants$x <- enfants$fu5_v1_par197_13
enfants$fu5_v1_par197_13 <- NULL
## enfants$x <- enfants$fu5_v1_par197_14
enfants$fu5_v1_par197_14 <- NULL
## enfants$x <- enfants$fu5_v1_par197_15
enfants$fu5_v1_par197_15 <- NULL
## enfants$x <- enfants$fu5_v1_par197_21
enfants$fu5_v1_par197_21 <- NULL
## enfants$x <- enfants$fu5_v1_par197_22
enfants$fu5_v1_par197_22 <- NULL
## enfants$x <- enfants$fu5_v1_par197_23
enfants$fu5_v1_par197_23 <- NULL
## enfants$x <- enfants$fu5_v1_par197_24
enfants$fu5_v1_par197_24 <- NULL
## enfants$x <- enfants$fu5_v1_par197_25
enfants$fu5_v1_par197_25 <- NULL
## enfants$x <- enfants$fu5_v1_par197_31
enfants$fu5_v1_par197_31 <- NULL
## enfants$x <- enfants$fu5_v1_par197_32
enfants$fu5_v1_par197_32 <- NULL
## enfants$x <- enfants$fu5_v1_par197_33
enfants$fu5_v1_par197_33 <- NULL
## enfants$x <- enfants$fu5_v1_par197_34
enfants$fu5_v1_par197_34 <- NULL
## enfants$x <- enfants$fu5_v1_par197_35
enfants$fu5_v1_par197_35 <- NULL
## enfants$x <- enfants$fu5_v1_par197_41
enfants$fu5_v1_par197_41 <- NULL
## enfants$x <- enfants$fu5_v1_par197_42
enfants$fu5_v1_par197_42 <- NULL
## enfants$x <- enfants$fu5_v1_par197_43
enfants$fu5_v1_par197_43 <- NULL
## enfants$x <- enfants$fu5_v1_par197_44
enfants$fu5_v1_par197_44 <- NULL
## enfants$x <- enfants$fu5_v1_par197_45
enfants$fu5_v1_par197_45 <- NULL
## enfants$x <- enfants$fu5_v1_par197_51
enfants$fu5_v1_par197_51 <- NULL
## enfants$x <- enfants$fu5_v1_par197_52
enfants$fu5_v1_par197_52 <- NULL
## enfants$x <- enfants$fu5_v1_par197_53
enfants$fu5_v1_par197_53 <- NULL
## enfants$x <- enfants$fu5_v1_par197_54
enfants$fu5_v1_par197_54 <- NULL
## enfants$x <- enfants$fu5_v1_par197_55
enfants$fu5_v1_par197_55 <- NULL
## enfants$x <- enfants$fu5_v1_par197_61
enfants$fu5_v1_par197_61 <- NULL
## enfants$x <- enfants$fu5_v1_par197_62
enfants$fu5_v1_par197_62 <- NULL
## enfants$x <- enfants$fu5_v1_par197_63
enfants$fu5_v1_par197_63 <- NULL
## enfants$x <- enfants$fu5_v1_par197_64
enfants$fu5_v1_par197_64 <- NULL
## enfants$x <- enfants$fu5_v1_par197_65
enfants$fu5_v1_par197_65 <- NULL

## enfants$x <- enfants$fu5_v1_par200
enfants$fu5_v1_par200 <- NULL
## enfants$x <- enfants$fu5_v1_par201
enfants$fu5_v1_par201 <- NULL
## enfants$x <- enfants$fu5_v1_par202
enfants$fu5_v1_par202 <- NULL
## enfants$x <- enfants$fu5_v1_par203
enfants$fu5_v1_par203 <- NULL
## enfants$x <- enfants$fu5_v1_par204
enfants$fu5_v1_par204 <- NULL
## enfants$x <- enfants$fu5_v1_par205
enfants$fu5_v1_par205 <- NULL
## enfants$x <- enfants$fu5_v1_par206
enfants$fu5_v1_par206 <- NULL
## enfants$x <- enfants$fu5_v1_par207
enfants$fu5_v1_par207 <- NULL
## enfants$x <- enfants$fu5_v1_par208
enfants$fu5_v1_par208 <- NULL
## enfants$x <- enfants$fu5_v1_par209
enfants$fu5_v1_par209 <- NULL
## enfants$x <- enfants$fu5_v1_par210
enfants$fu5_v1_par210 <- NULL
## enfants$x <- enfants$fu5_v1_par211
enfants$fu5_v1_par211 <- NULL
## enfants$x <- enfants$fu5_v1_par212
enfants$fu5_v1_par212 <- NULL
## enfants$x <- enfants$fu5_v1_par213
enfants$fu5_v1_par213 <- NULL
## enfants$x <- enfants$fu5_v1_par214
enfants$fu5_v1_par214 <- NULL
## enfants$x <- enfants$fu5_v1_par215
enfants$fu5_v1_par215 <- NULL
## enfants$x <- enfants$fu5_v1_par216
enfants$fu5_v1_par216 <- NULL
## enfants$x <- enfants$fu5_v1_par217
enfants$fu5_v1_par217 <- NULL
## enfants$x <- enfants$fu5_v1_par218
enfants$fu5_v1_par218 <- NULL
## enfants$x <- enfants$fu5_v1_par219
enfants$fu5_v1_par219 <- NULL
## enfants$x <- enfants$fu5_v1_par220
enfants$fu5_v1_par220 <- NULL
## enfants$x <- enfants$fu5_v1_par221
enfants$fu5_v1_par221 <- NULL
## enfants$x <- enfants$fu5_v1_par222
enfants$fu5_v1_par222 <- NULL
## enfants$x <- enfants$fu5_v1_par223
enfants$fu5_v1_par223 <- NULL
## enfants$x <- enfants$fu5_v1_par224
enfants$fu5_v1_par224 <- NULL
## enfants$x <- enfants$fu5_v1_par225
enfants$fu5_v1_par225 <- NULL
## enfants$x <- enfants$fu5_v1_par226
enfants$fu5_v1_par226 <- NULL
## enfants$x <- enfants$fu5_v1_par227
enfants$fu5_v1_par227 <- NULL
## enfants$x <- enfants$fu5_v1_par228
enfants$fu5_v1_par228 <- NULL
## enfants$x <- enfants$fu5_v1_par229
enfants$fu5_v1_par229 <- NULL
## enfants$x <- enfants$fu5_v1_par230
enfants$fu5_v1_par230 <- NULL
## enfants$x <- enfants$fu5_v1_par231
enfants$fu5_v1_par231 <- NULL

## enfants$x <- enfants$fu5_v1_par232_11
enfants$fu5_v1_par232_11 <- NULL
## enfants$x <- enfants$fu5_v1_par232_12
enfants$fu5_v1_par232_12 <- NULL
## enfants$x <- enfants$fu5_v1_par232_13
enfants$fu5_v1_par232_13 <- NULL
## enfants$x <- enfants$fu5_v1_par232_21
enfants$fu5_v1_par232_21 <- NULL
## enfants$x <- enfants$fu5_v1_par232_22
enfants$fu5_v1_par232_22 <- NULL
## enfants$x <- enfants$fu5_v1_par232_23
enfants$fu5_v1_par232_23 <- NULL
## enfants$x <- enfants$fu5_v1_par232_31
enfants$fu5_v1_par232_31 <- NULL
## enfants$x <- enfants$fu5_v1_par232_32
enfants$fu5_v1_par232_32 <- NULL
## enfants$x <- enfants$fu5_v1_par232_33
enfants$fu5_v1_par232_33 <- NULL
## enfants$x <- enfants$fu5_v1_par234
enfants$fu5_v1_par234 <- NULL
## enfants$x <- enfants$fu5_v1_par235_11
enfants$fu5_v1_par235_11 <- NULL
## enfants$x <- enfants$fu5_v1_par235_12
enfants$fu5_v1_par235_12 <- NULL
## enfants$x <- enfants$fu5_v1_par235_13
enfants$fu5_v1_par235_13 <- NULL
## enfants$x <- enfants$fu5_v1_par235_21
enfants$fu5_v1_par235_21 <- NULL
## enfants$x <- enfants$fu5_v1_par235_22
enfants$fu5_v1_par235_22 <- NULL

## enfants$x <- enfants$fu5_v1_par262
enfants$fu5_v1_par262 <- NULL
## enfants$x <- enfants$fu5_v1_par263
enfants$fu5_v1_par263 <- NULL
## enfants$x <- enfants$fu5_v1_par264
enfants$fu5_v1_par264 <- NULL
## enfants$x <- enfants$fu5_v1_par265
enfants$fu5_v1_par265 <- NULL
## enfants$x <- enfants$fu5_v1_par266
enfants$fu5_v1_par266 <- NULL
## enfants$x <- enfants$fu5_v1_par267
enfants$fu5_v1_par267 <- NULL
## enfants$x <- enfants$fu5_v1_par268
enfants$fu5_v1_par268 <- NULL
## enfants$x <- enfants$fu5_v1_par269
enfants$fu5_v1_par269 <- NULL
## enfants$x <- enfants$fu5_v1_par274
enfants$fu5_v1_par274 <- NULL
## enfants$x <- enfants$fu5_v1_par275
enfants$fu5_v1_par275 <- NULL


## Catch-all clean-up
enfants$x <- NULL
```
