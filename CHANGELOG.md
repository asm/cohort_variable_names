# Changelog


The following is a rough guide to versions:

 * Versions 0.x : based on theoretical discussion about best naming
   options
   - Versions up to 0.5 : private (within the RECAP Preterm) versions
   - Version 0.6 : first public version
   
 * Versions 1.x : development versions of the variable naming scheme,
   for use in initial projects.

 * Versions 2.x or higher : stable versions of the variable naming
   scheme, suitable for use in preterm and child birth cohorts.

The following sections contain more detailed information on
versions. They are listed in reverse order (i.e. most recent first)

## Version 1.1
__2021-01-05__:
 * [background](background.md)
   - added line about not including '`-`' symbol in name.
   - formatting changes (adding apostrophes around symbols).
   

## Version 1.0 

__2020-10-23__: First release of naming scheme, used in RECAP Preterm
demonstration projects. Updates in this version particularly relate to
variable names to be used in relation to follow-up
questionnaire. Particular thanks to Marina Goulart de Mendonça, Mariane
Sentenac and Sylvia van der Pal who provided notes and examples based on
their work (and coordinated discussions within their respective teams).

 * [`naming_scheme`](/naming_scheme.md)
   - included description of questionnaire respondents.
   - link to 'questionnaires' page.

 * [`variable`](/variable.md)
   - included 'Source' section for questionnaire respondents.
   - edited other sections to detail the revised 'questionnaire' format.
   - provided examples of questionnaire codes.
 
 * [questionnaires](/questionnaires.md)
   - created page.
   - provided description about specifics for follow-up questionnaires.

 * [questionnaire_codes](/questionnaire_codes.md)
   - created page.
   - included questionnaire codes.
 
 * [CONTRIBUTORS](/CONTRIBUTORS.md)
   - Added people who've participated in meetings and/or mailed
     contributions.
   
## Version 0.7

 *  [`subject`](/subject.md): added description about non-human subject
   entities.
   
 * [`unit`](/unit.md): 
   - added denomination for multiple categories within a ordinal or
     nominal variable.
   - description about time units.
   - description about compound variables.

 * [`timepoint`](/timepoint.md): 
   - expanded list of fixed time points.
   - abbreviated antenatal and neonatal.
   - discussion about 'origin' of time scale.
   
 * [examples](/examples.md) document updated with examples from
   EPIPAGE-2
   
   
## Version 0.6

 * First public version
 * Converted to markdown and published in a git repository
 * All documents written
 
## Version 0.5

 * Developed generic format for naming variables
 
## Versions 0.4 and previous

 * Initial versions developed by Stef van Buuren within the RECAP
   Preterm project.
