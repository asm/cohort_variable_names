# Variable

The `variable` component represents the main part of the variable name,
and has two peculiarities which the other components of the naming
scheme do not have:

 * it is the only obligatory component;
 * it may optionally be divided into two components (separated by an
   underscore, `_`), representing 'major' and 'minor' parts of the
   variable name; further, the 'major' component can also have a
   'source' component appended to it (using a period, `.`) to indicate
   who the respondent is for a questionnaire.
   
If the `variable` component is divided into two, the first, 'major' part
normally represents the main stem of the variable, and the second,
'minor' part represents the specific branch.  However, in the case of
variables from a questionnaire, the first, 'major' part indicates the
questionnaire itself (along with the respondent) and the second, 'minor'
part indicates the item name. 

## Major variable component

This is the main (and compulsory) stem of the variable name. It may
either be: unique; correspond to a class of variables which are then
further classified through use of the 'minor variable' component; or
relate to a questionnaire - in which case it may also need to contain to
subject code and/or version number.

Examples of unique variable names are:

 * `id` : for unique identifier values for subjects
 * `sex` : for biological sex
 * `wgt` : for weight
 * `hgt` : for height
 
Examples of classes of variables are:

 * `vent_` : representing respiratory support (e.g. during the neonatal
   period)
 * `pda_` : representing variables relating to a __patent ductus
   arteriosus__
 * `fam_` : representing variables relating to the family unit of
   which the subject is a member
 
Further examples of how these may be constructed are provided in the
section below.

Example of [questionnaire codes](/questionnaire_codes.md) are:

 * `asq3`:  Ages and Stages Questionnaire, 3rd edition
 * `bfi`: Big Five personality traits
 * `bsid2`: Bayley Scales of Infant Development, 2nd edition
 * `bsid3`: Bayley Scales of Infant Development, 3rd edition
 * `hui`: Health Utilities Index
 * `hui2`: Health Utilities Index-2
 * `hui3`: Health Utilities Index-3
 * `wppsi4`: Wechsler Preschool and Primary Scale of Intelligence, 4th
   edition
 
A more complete list is provided on the
[questionnaires](/questionnaires.md) page.


## Source

If the variable refers to a follow-up questionnaire, then the
`majorvariable` field refers to the test used and the information
`source` (i.e. whoever the respondent to the questionnaire is, rather
than the subject of the questionnaire) is added after a dot (`.`). This
should be:

 * s = self report (this is only required if the questionnaire could
   also be responded to by somebody else; if it is otherwise implicit,
   it should _not_ be included)
 * p = parent report
 * t = teacher report
 * e = examiner report
 * m = medical report

Examples of sources for the Health Utilities Index are:

 * `hui.p`: health utilities index as reported by the child's parent
 * `hui.t`: health utilities index as reported by the child's teacher

## Minor variable component

This component of the variable name relates to a minor branch that may
be required to distinguish individual variables within a general class
of variable.

Examples, in relation to the major stem components described above are:

 - for neonatal respiratory support
   * `vent_cpap` : representing ventilatory support with continuous
     positive airway pressure (CPAP)
   * `vent_mech` : representing mechanical (invasive ventilation)
     respiratory support
   * `vent_ninv` : representing non-invasive respiratory support
   * `vent_oxy` : representing respiratory support with supplementary
     oxygen

 - for variables relating to patent ductus arterious (PDA)
   * `pda_med` : relating to medical treatment of a PDA
   * `pda_surg` : relating to surgical treatment of a PDA
   
 - for variables relating to the family unit
   * `fam_incm` : monthly family income
   * `fam_incy` : annual (yearly) family income
   * `fam_wlth` : 
   * `fam_size` : family size (number of family members cohabiting in
     the household)

For questionnaires, the minor variable component is used to indicate the
score or the item that is being assessed. For example:

 * `by2.e_mdi`: MDI score derived from the BSID-II, based on clinical examination
 * `kabc.e_mpc`: MPC score derived from the KABC, based on clinical examination
 
More (and complete) examples of the use 

---- 

Return to [`naming_scheme`](/naming_scheme.md) main page.
