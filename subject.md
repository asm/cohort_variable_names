# Subject

The `subject_` part of the variable name is required for all subjects
_except_ the index subject. **If no `subject_` component is provided, it
is implicit that the variable relates to the index subject.**

It should also be noted that a subject may not be human, as discussed
below.

## Unique subjects' variable codes

Certain family members are unique - for example, individuals normally
only have one biological mother and father (although they may also have
adopted or step parents), and one set of maternal and one set of
paternal grandparents. It is therefore possible to give these subjects
unique codes, as shown in the following table.

 | Family member | variable name   |
 | ------------- | --------------- |
 | Mother        | `mat_`          |
 | Father        | `pat_`          |
 | Stepmother    | `smat_`         | 
 | Stepfather    | `spat_`         |
 | Adoptive mother | `amat_`       |
 ! Adoptive father | `apat_`       |
 | Maternal grandmother | `matgm_` |
 | Paternal grandmother | `patgm_` |
 | Maternal grandfather | `matgf_` |
 | Paternal grandfather | `patgf_` |


## Non-unique subjects' variable codes

In contrast, there may be more than one sibling, or the index subject
may have more than one child of their own. These subjects should
therefore be denoted with the suffix _`X`_ appended onto the root of the
subject term. Specifically, this gives the following syntax:

 * `sib`_`X`_`_` - e.g. `sib1_`, `sib2_`, etc
 * `child`_`X`_`_` - e.g. `child_`, `child2_`, etc
 
It should be noted that this numeric identifier does _not_ indicate
birth order of those subjects, but is an arbitrary assignation intended
to provide consistency in ensuring different variables relate to the
same subject. This is particularly true for twins or other multiple
births, but may also apply if not all family members are initially known
about.

As above, the non-unique family members may also be adopted or step
relatives. These would be indicated with prefixes of `a` or `s`
respectively:

 * `asib`_`X`_`_` : for adopted siblings 
 * `ssib`_`X`_`_` : for step siblings

 * `achild`_`X`_`_` : for adopted children 
 * `schild`_`X`_`_` : for step children
 


## Non-human subjects

Subjects may also be non-human entities - for example, organisations
such as hospitals or schools, geographical areas such as countries or
regions (within countries), or animals such as cats or dogs.

These may require a suffix to indicate if they are non-unique
(e.g. `dog`_`X`_) or be associated with a [`timepoint`](/timepoint.md)
component (e.g. `hosp_id_birth` for hospital of birth may be different
from `hosp_id_nicadmit` - for the hospital where the child is first
admitted to NICU).


## Notes and conventions

From the above, certain conventions are apparent:

 * `a` is used in relation to adoptive / adopted relatives
 * `s` is used in relation to step relatives

Further, the convention for historical (previous) generations is:
 
 * `mat` is the mother / maternal side of the family
 * `pat` is the father / paternal side of the family

This could instead be shortened to `m` and `p` respectively, however
this breaks from the [conventions](background.md) that suggest variable
names should a) use pronouncable names, and b) conform to well-accepted
definitions and names. Retaining the longer version is also acceptable
as virtually all the suggested subject notations are within the soft 5
character component limit that is advised in order to keep variable
names below 30 characters in length.
 
---- 

Return to [`naming_scheme`](/naming_scheme.md) main page.
