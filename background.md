# Background

The variable naming scheme was initially developed as part of the
European Union Horizon 2020 funded [RECAP
Preterm](https://recap-preterm.eu) project in order to provide
standardised variable names for use in harmonised data sets used for
federated data analysis. Objectives and preferred conventions are
described below.

## Objectives:
 * To enable federated data analyses
 * To enhance clarity in cases of potential ambiguity
 * To help avoid "naming collisions" that might occur when the work product of different organisations is combined
 * To provide better understanding in case of code reuse after a long interval of time
 * To reduce the effort needed to read and understand source code
	
## Conventions:
 * Prefer shorter, descriptive, lowercase names
 * Prefer not to exceed 30 places (including underscores)
 * Names cannot contain spaces: where needed, use underscore '`_`' to connect words
 * Names cannot include the '`-`' symbol (potential confusion with 'minus'); should generally omit
 * Use pronouncable names
 * Conform to conventions used in SPSS, R, Stata and python
 * Should not end in a '`.`' or '`_`', should not start with '`$`', '`_`' or '`.`'
 * Where sensible, conform to well-accepted definitions and names

## Special cases:

 * Same name for same variable at different time points
 * Use equal length systematic names for sets of related variables



## Useful documentation and further reading

 * Draft Harmonisation Guidelines for WP7 topic leads, compiled by ULEIC, V1_12/03/2019, (RECAP internal document)
 * https://en.wikipedia.org/wiki/Naming_convention_(programming)
 * http://bensmith.io/20-tips-for-better-naming

